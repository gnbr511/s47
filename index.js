const text_first_name = document.querySelector('#text-first-name')
const text_last_name = document.querySelector('#text-last-name')
const span_full_name = document.querySelector('#span-full-name')


// const concatFnameLname = () => {
// 	span_full_name.innerHTML = text_first_name.value + " "
// 	span_full_name.innerHTML += text_last_name.value
// }

text_first_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = text_first_name.value
})
text_last_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = text_first_name.value + " "
	span_full_name.innerHTML += text_last_name.value
})

text_first_name.addEventListener('keyup', (event) => {
	console.log(event.target)
	console.log(event.target.value)
	
	//event.target is just the same as the current element. Which means this code below will also work the same way.
	// console.log(text_first_name)
	// console.log(text_first_name.value)
})